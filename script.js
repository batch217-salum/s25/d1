// console.log("Hello World!");

// [section] json object
// Javascript object notation


// JSON Object

/*{
	"city" : " Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}*/

// JSON Array

//arrayName = cities

/*"cities" : [
	{
		"city" : "Quezon City", 
		"province" : "Metro Manila", 
		"country" : "Philippines"
	},
	{
		"city" : "Manila City",
		"province" : "Metro Manila",
		"country" : "Philippines"
 	},
 	{
		"city" : "Makati City",
		"province" : "Metro Manila",
		"country" : "Philippines"
 	}
]*/

// [Section] JSON Methods

let batchesArr = [
	{
		batchName : "Batch 217",
		schedule: "Full-Time"
	},
	{
		batchName : "Batch 218",
		schedule : "Part-Time"
	}
]

// stringify --> converting method for JS objects into a string
console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name : "John",
	age : 31,
	address : {
		city : "Manila",
		country : "Philippines"
	}
});

console.log(data);

// [Section] Using Stringify Method with Variables 

let firstName = prompt("What is your First Name?");
let lastName = prompt("What is your Last Name?");
let age = prompt("What is your age?");
let address = {
	city : prompt("Which city do you live in?"),
	country : prompt("Which country does your city belong to?")
};

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(otherData);

// [Section] Converting Stringified JSON into JS Objects

//Parse Method

let batchesJSON = `[
	{
		"batchName" : "Batch 217",
		"schedule" : "Full-Time"	
	},
	{
		"batchName" : "Batch 218",
		"schedule" : "Part-Time"
	}
]`
console.log("Result from prse method:");
console.log(JSON.parse(batchesJSON));


let stingifiedObject = `{
	"name" : "John",
	"age" : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}
}`


console.log(JSON.parse(stingifiedObject));